# BotBrother

Is a 'big brother' bot that monitors Amazon EC2 instances, and sends a slack
message to the owner of that instance if the spawn date was more then 2 days ago.
