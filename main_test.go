package main

import (
	"github.com/aws/aws-sdk-go/service/ec2"
	"testing"
)

func TestLoadingOfJson(t *testing.T) {
	configs := loadconfig()
	if configs == nil {
		t.Error("Should be able to load configs")
	}
	if len(configs) != 2 {
		t.Error("Was expecting 2 configs, but got ", len(configs))
	}
}

func TestLoadingOfJson1(t *testing.T) {
	configs := loadconfig()
	if configs == nil {
		t.Error("Should be able to load configs")
	}
	key := "Purpose"
	value := "IT Node"
	tags := []*ec2.Tag{&ec2.Tag{Key: &key, Value: &value}}
	time := expTime(configs, tags, 100)
	if time != 10 {
		t.Error("Expire time should have been 10")
	}

}
func TestResolveToNonExistingValueShouldReturnDefault(t *testing.T) {
	configs := loadconfig()
	if configs == nil {
		t.Error("Should be able to load configs")
	}
	key := "Purpose"
	value := "Nothing"
	tags := []*ec2.Tag{&ec2.Tag{Key: &key, Value: &value}}
	time := expTime(configs, tags, 100)
	if time != 100 {
		t.Error("Expire time should have been 10")
	}

}
